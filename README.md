# EMP-chromium

Adds chromium in headless mode to EMP image

## Tags

* latest - testing build, can vary (v1, v2)
* v1 - mariadb ~10.4 with php ~7.2 on alpine linux 3.11
* v2 - mariadb ~10.4 with php ~7.4 on alpine linux 3.14