FROM cloudtecbern/emp:v1

COPY conf/chromium-headless /etc/init.d/chromium-headless
ADD scripts/healthcheck/* /scripts/healthcheck/
ADD scripts/startup/* /scripts/startup/

RUN chmod -R 0755 /scripts \
  && chmod 0755 /etc/init.d/chromium-headless \
  \
### INSTALL PACKAGES ###################################################################################################
  && apk add --update --no-cache \
     libatomic libxt libxmu xset xprop xdg-utils chromium chromium-chromedriver