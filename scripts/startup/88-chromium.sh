#!/bin/bash

#
# Run MariaDB daemon in background.
#
# Documentation and original MariaDB init scripts:
# - https://raw.githubusercontent.com/yobasystems/alpine-mariadb/master/alpine-mariadb-amd64/files/run.sh
# - https://wiki.alpinelinux.org/wiki/MariaDB
#

/etc/init.d/chromium-headless start